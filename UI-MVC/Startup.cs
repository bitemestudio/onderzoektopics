﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SC.UI.Web.MVC.Models;
using SC.UI.Web.MVC.Models.DTOs;
using SignalRChat.Hubs;
using Swashbuckle.AspNetCore.Swagger;

namespace SC.UI.Web.MVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc()
                .AddHateoas(
                    options =>
                    {
                        options.AddLink<List<TicketDTO>>("get-tickets")
                            .AddLink<List<TicketDTO>>("create-ticket")
                            .AddLink<TicketDTO>("get-ticket", p => new {id = p.TicketNumber})
                            .AddLink<TicketDTO>("update-ticket", p => new {id = p.TicketNumber})
                            .AddLink<TicketDTO>("delete-ticket", p => new {id = p.TicketNumber});
                    }
                )
                .AddXmlSerializerFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });

            services.AddSignalR();
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(300);
                options.Cookie.HttpOnly = true;
            });
            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddSwaggerGen(
                c =>
                {
                    c.SwaggerDoc("v1.0", new Info
                    {
                        Version = "v1.0",
                        Title = "Support Center API",
                        Description = "Supportcenter REST Documentatie",
                        TermsOfService = "None",
                        Contact = new Contact
                        {
                            Name = "Thomas Evers & Stijn Bogaerts",
                            Email = "Thomas.Evers@Student.KdG.be",
                        },
                        License = new License
                        {
                            Name = "Karel de Grote",
                            Url = "https://www.kdg.be/"
                        },
                    });
                    c.DescribeAllEnumsAsStrings();
                    c.IncludeXmlComments(
                        Path.ChangeExtension(Assembly.GetEntryAssembly().Location, "xml")
                    );
                    
                }
            );
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(
                    c =>
                    {
                      c.SwaggerEndpoint("v1.0/swagger.json", "SupportCenter API V1");
                    }
                );
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSession();



            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
            });


            app.UseSignalR(routes => { routes.MapHub<ChatHub>("/chatHub"); });
            app.UseSignalR(routes => { routes.MapHub<ChatHub>("/chatHub"); });
        }
    }
}