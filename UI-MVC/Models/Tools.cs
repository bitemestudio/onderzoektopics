using System.Collections;
using SC.UI.Web.MVC.Models;

namespace SC.UI.Web.MVC.Controllers
{
    public class Tools
    {
        public static ArrayList getUserList()
        {
            var users = new ArrayList();

            users.Add(new User(1, "Bob", true));
            users.Add(new User(2, "Jozefien", false));
            users.Add(new User(3, "Bernadet", false));
            users.Add(new User(4, "Stijn", true));
            users.Add(new User(5, "Thomas", true));
            users.Add(new User(6, "Mats", false));
            users.Add(new User(7, "Tom", false));
            users.Add(new User(8, "Jeroen", false));
            users.Add(new User(9, "Laura", false));
            users.Add(new User(10, "Kenneth", true));

            return users;
        }
    }
}