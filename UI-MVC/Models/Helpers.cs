using System;
using System.Collections.Generic;
using AutoMapper;
using SC.BL.Domain;
using SC.UI.Web.MVC.Models.DTOs;

namespace SC.UI.Web.MVC.Models
{
    public class Helpers
    {
        private readonly IMapper _mapper;

        public Helpers(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Ticket ConvertToTicket(Ticket ticket, TicketDTO patchDto)
        {
            if (patchDto.Text != null) ticket.Text = patchDto.Text;

            if (patchDto.AccountId != null) ticket.AccountId = (int) patchDto.AccountId;

            if (patchDto.State != null) ticket.State = (TicketState) patchDto.State;

            if (patchDto.DateOpened != null) ticket.DateOpened = (DateTime) patchDto.DateOpened;

            return ticket;
        }

        public List<TicketDTO> ConvertToTicketDtos(IEnumerable<Ticket> tickets)
        {
            var ticketDtoList = new List<TicketDTO>();
            foreach (var ticket in tickets) ticketDtoList.Add(ConvertToTicketDto(ticket));

            return ticketDtoList;
        }

        public TicketDTO ConvertToTicketDto(Ticket ticket)
        {
            return _mapper.Map<TicketDTO>(ticket);
        }
    }
}