namespace SC.UI.Web.MVC.Models
{
    public class User
    {
        public User(int number, string username, bool adm)
        {
            id = number;
            name = username;
            admin = adm;
        }

        public int id { get; set; }
        public string name { get; set; }
        public bool admin { get; set; }
    }
}