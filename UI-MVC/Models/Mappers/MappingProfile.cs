using AutoMapper;
using SC.BL.Domain;
using SC.UI.Web.MVC.Models.DTOs;

namespace SC.UI.Web.MVC.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TicketDTO, Ticket>();
            CreateMap<Ticket, TicketDTO>();
        }
    }
}