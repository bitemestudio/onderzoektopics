using System;
using SC.BL.Domain;

namespace SC.UI.Web.MVC.Models
{
    public class UpdateObject
    {
        public int UpdatedTicketNumber { get; set; }
        public int UpdatedAccountId { get; set; }
        public string UpdatedText { get; set; }
        public DateTime UpdatedDateOpened { get; set; }
        public TicketState UpdatedState { get; set; }

        public UpdateObject()
        {
        }
    }
}