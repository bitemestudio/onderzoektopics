using System;

namespace SC.UI.Web.MVC.Models.DTOs
{
    public class ResponseDTO
    {
        public int? Id { get; set; }
        public string Text { get; set; }
        public DateTime? Date { get; set; }
        public bool? IsClientResponse { get; set; }
    }
}