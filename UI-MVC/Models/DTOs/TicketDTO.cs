using System;
using SC.BL.Domain;

namespace SC.UI.Web.MVC.Models.DTOs
{
    public class TicketDTO
    {
        public string message = "test";
        public int? TicketNumber { get; set; }
        public int? AccountId { get; set; }
        public string Text { get; set; }
        public DateTime? DateOpened { get; set; }
        public TicketState? State { get; set; }
    }
}