namespace SC.UI.Web.MVC.Models.DTOs
{
    public class NewTicketDTO
    {
        public int AccountId { get; set; }
        public string Text { get; set; }
    }
}