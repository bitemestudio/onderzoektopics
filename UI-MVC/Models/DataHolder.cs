using System.Collections;
using System.Collections.Generic;

namespace SC.UI.Web.MVC.Models
{
    public static class DataHolder
    {
        public static List<user> UserDataList = new List<user>();
        public static ArrayList connectionIds = new ArrayList();

        public struct user
        {
            public string name { get; set; }
            public bool admin { get; set; }
        }
    }
}