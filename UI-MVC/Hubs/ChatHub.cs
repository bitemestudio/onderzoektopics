﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SC.UI.Web.MVC.Models;

namespace SignalRChat.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string message)
        {
            var name = "";
            for (var i = 0; i < DataHolder.connectionIds.Count; i++)
                if (DataHolder.connectionIds[i].Equals(Context.ConnectionId))
                    name = DataHolder.UserDataList[i].name;
            await Clients.All.SendAsync("Send", name + " says: " + message);
        }

        public async Task SendMessageToGroup(string groupName, string message)
        {
            var name = "";
            for (var i = 0; i < DataHolder.connectionIds.Count; i++)
                if (DataHolder.connectionIds[i].Equals(Context.ConnectionId))
                    name = DataHolder.UserDataList[i].name;
            await Clients.Group(groupName).SendAsync("ReceiveMessage", groupName, message);
        }

        public async Task AddToGroup(string groupName)
        {
            var name = "";
            var admin = false;
            for (var i = 0; i < DataHolder.connectionIds.Count; i++)
                if (DataHolder.connectionIds[i].Equals(Context.ConnectionId))
                {
                    name = DataHolder.UserDataList[i].name;
                    admin = DataHolder.UserDataList[i].admin;
                }

            if (admin)
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
                await Clients.All.SendAsync("Send", name + " has joined the group " + groupName);
            }
        }

        public async Task RemoveFromGroup(string groupName)
        {
            var name = "";
            var admin = false;
            for (var i = 0; i < DataHolder.connectionIds.Count; i++)
                if (DataHolder.connectionIds[i].Equals(Context.ConnectionId))
                {
                    name = DataHolder.UserDataList[i].name;
                    admin = DataHolder.UserDataList[i].admin;
                }

            if (admin)
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
                await Clients.All.SendAsync("Send", name + " has left the group " + groupName);
            }
        }


        public override Task OnConnectedAsync()
        {
            var name = "";
            var admin = false;
            DataHolder.connectionIds.Add(Context.ConnectionId);
            for (var i = 0; i < DataHolder.connectionIds.Count; i++)
                if (DataHolder.connectionIds[i].Equals(Context.ConnectionId))
                {
                    name = DataHolder.UserDataList[i].name;
                    admin = DataHolder.UserDataList[i].admin;
                }

            if (!admin)
            {
                Groups.AddToGroupAsync(Context.ConnectionId, name);
                Clients.Group(name).SendAsync("Send", "A user has joined the group " + name);
                Clients.All.SendAsync("AddAdmin", name);
            }
            else
            {
                Clients.All.SendAsync("Send", "new admin joined: " + name);
            }
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            string name;
            DataHolder.connectionIds.Add(Context.ConnectionId);
            for (var i = 0; i < DataHolder.connectionIds.Count; i++)
                if (DataHolder.connectionIds[i].Equals(Context.ConnectionId))
                {
                    if (DataHolder.UserDataList[i].admin) //admin
                    {
                        DataHolder.connectionIds.RemoveAt(i);
                    }
                    else
                    {
                        name = DataHolder.UserDataList[i].name;
                        //client
                        DataHolder.connectionIds.RemoveAt(i);
                        Clients.Group(name)
                            .SendAsync("RemoveAdmin", name);
                        Groups.RemoveFromGroupAsync(Context.ConnectionId, name);
                    }
                }

            return base.OnDisconnectedAsync(exception);
        }
    }
}