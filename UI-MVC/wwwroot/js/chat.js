﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();
//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;
connection.onclose(reconnect);

function reconnect () {
    console . log ( 'reconnecting...' );
    setTimeout (startConnection, 2000);
}
function startConnection () {
    console . log ( 'connecting...' );
    connection.start()
        . then (() => console . log ( 'connected!' ))
. catch (reconnect);
}
connection.on("ReceiveMessage", function (group, message) {
    //admin
    try {
        var li = document.createElement("li");
        li.textContent = message;
        var chat =document.getElementById(group);
        chat.appendChild(li);    }
    catch(err) {
        console.log(err.toString());
    }

    //client
    try {
        var li = document.createElement("li");
        li.textContent = message;
        var chat = document.getElementById("messagesListclient");
        chat.appendChild(li);
    }
    catch(err) {
        console.log(err.toString());
    }

});
connection.on("AddAdmin", function (groupName) {
    //only creates layout for admins
    try {
        connection.invoke("AddToGroup", groupName);
        var ul = document.createElement("lu");
        var div = document.createElement("section");
        div.setAttribute("id",groupName+"item");
        div.setAttribute("class", "accordion");
        ul.appendChild(div);
        var toggle = document.createElement("input");
        toggle.setAttribute("type", "checkbox");
        toggle.setAttribute("name", "collapse");
        toggle.setAttribute("id", "handle");
        toggle.setAttribute("checked", "checked");
        div.appendChild(toggle);
        var head = document.createElement("h2");
        head.setAttribute("class", "handle");
        div.appendChild(head);
        var label = document.createElement("label");
        label.textContent = groupName;
        label.setAttribute("for", "handle");
        head.appendChild(label);

        var content = document.createElement("div");
        content.setAttribute("class", "content");

        div.appendChild(content);
        var lu = document.createElement("ul");
        lu.setAttribute("id", groupName);
        content.appendChild(lu);
        var text = document.createElement("li");
        text.textContent = "chat with " + groupName;
        lu.appendChild(text);
        document.getElementById("chats").appendChild(div);
        var input=document.createElement("input");
        input.setAttribute("type","text");
        input.setAttribute("id",groupName+"input");
        input.setAttribute("placeholder","message");
        content.appendChild(input);
        var btn = document.createElement("BUTTON");
        btn.setAttribute("class","btn");
        btn.setAttribute("for",groupName+"input");
        btn.onclick=function() {
            console.log("button in chat clicked");
            try {
                var msg= document.getElementById(groupName+"input").value;
                connection.invoke("SendMessageToGroup",groupName ,"admin says: "+ msg);
            }
            catch (e) {
                console.log(e.toString());
            }
        };
        var t = document.createTextNode("send to group");
        btn.appendChild(t);
        content.appendChild(btn);
    }
    catch(err) {
        console.log(err.toString());
    }
    try{
        document.getElementById("myname").textContent=groupName;
    }
    catch (e) {
        console.log(e.toString());
    }

});
connection.on("RemoveAdmin", function (groupName) {
    //only creates layout for admins
    console.log("called removeadmin");
    try {
        try {
            connection.invoke("RemoveFromGroup", groupName);
            var element=document.getElementById(groupName+"item");
            element.parentElement.removeChild(element);
        }
        catch (e) {
            console.error(e.toString());
        }
        event.preventDefault();
    }
    catch(err) {
        console.log(err.toString());
    }

});
connection.on("Send", function (message) {
    var li = document.createElement("li");
    li.textContent = message;
    document.getElementById("messagesList").appendChild(li);
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;

}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});
try {
    document.getElementById("leave-group").addEventListener("click", function (event) {
        var groupName = document.getElementById("group-name").value;
        try {
            connection.invoke("RemoveFromGroup", groupName);
            var element=document.getElementById(groupName+"item");
            element.parentElement.removeChild(element);

        }
        catch (e) {
            console.error(e.toString());
        }
        event.preventDefault();
    });
}
catch (e) {

}

try{
    document.getElementById("join-group").addEventListener("click", function (event) {
        try {
            var groupName=document.getElementById("group-name").value;
            connection.invoke("AddToGroup", groupName);
            var ul = document.createElement("lu");
            var div = document.createElement("section");
            div.setAttribute("id",groupName+"item");
            div.setAttribute("class", "accordion");
            ul.appendChild(div);
            var toggle = document.createElement("input");
            toggle.setAttribute("type", "checkbox");
            toggle.setAttribute("name", "collapse");
            toggle.setAttribute("id", "handle");
            toggle.setAttribute("checked", "checked");
            div.appendChild(toggle);
            var head = document.createElement("h2");
            head.setAttribute("class", "handle");
            div.appendChild(head);
            var label = document.createElement("label");
            label.textContent = groupName;
            label.setAttribute("for", "handle");
            head.appendChild(label);

            var content = document.createElement("div");
            content.setAttribute("class", "content");

            div.appendChild(content);
            var lu = document.createElement("ul");
            lu.setAttribute("id", groupName);
            content.appendChild(lu);
            var text = document.createElement("li");
            text.textContent = "chat with " + groupName;
            lu.appendChild(text);
            document.getElementById("chats").appendChild(div);
            var input=document.createElement("input");
            input.setAttribute("type","text");
            input.setAttribute("id",groupName+"input");
            input.setAttribute("placeholder","message");
            content.appendChild(input);
            var btn = document.createElement("BUTTON");
            btn.setAttribute("class","btn");
            btn.setAttribute("for",groupName+"input");
            btn.onclick=function() {
                try {
                    var msg= document.getElementById(groupName+"input").value;
                    connection.invoke("SendMessageToGroup",groupName ,"admin says: "+ msg);
                }
                catch (e) {
                    console.log(e.toString());
                }
            };
            var t = document.createTextNode("send to group");
            btn.appendChild(t);
            content.appendChild(btn);
        }
        catch(err) {
            console.log(err.toString());
        }
        event.preventDefault();
    });
}
catch (e) {
    console.log(e.toString());
}


try{
    document.getElementById("groupmsgclient").addEventListener("click", function (event)  {
        var groupMsg = document.getElementById("messageInput").value;
        try {
            connection.invoke("SendMessageToGroup",document.getElementById("myname").textContent ,document.getElementById("myname").textContent+" says: "+  groupMsg);
        }
        catch (e) {
            console.log(e.toString());
        }
        event.preventDefault();
    });
}
catch (err) {
    console.log(err.toString());
}