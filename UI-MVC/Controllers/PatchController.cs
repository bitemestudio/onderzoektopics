using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;
using SC.UI.Web.MVC.Models;
using SC.UI.Web.MVC.Models.DTOs;

namespace SC.UI.Web.MVC.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PatchController :Controller
    {
        private readonly IMapper _mapper;
        private readonly Helpers _helper;
        private readonly ITicketManager _mgr = new TicketManager();

        public PatchController(IMapper mapper)
        {
            _mapper = mapper;
            _helper = new Helpers(_mapper);
        }
        /// <summary>
        /// Edit ticket.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     DELETE /api/hateoas/1
        /// </remarks>
        /// <param name="id">UID voor ticket dat gewijzigd dient te worden.</param>
        /// <param name="patchDto">ticket-object met de waardes die gewijzigd moeten worden.</param>
        /// <returns>Het gewijzigde ticket.</returns>
        /// <response code="200">Returns "Ok" terug als het ticket is gewijzigd.</response>
        /// <response code="400">returns "BadRequest" wanneer er geen  ticket met id gevonden word.</response>

        [ProducesResponseType(typeof(Ticket), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPatch("replace/{id}")]
        public IActionResult Replace(int id, [FromBody] TicketDTO patchDto)
        {
            var ticket = _mgr.GetTicket(id);
            if (ticket == null) return BadRequest();

//            var helper = new Helpers();
//            mgr.ChangeTicket(helper.ConvertToTicket(ticket, patchDto));
            return Ok(_mgr.ChangeTicket(_helper.ConvertToTicket(ticket, patchDto)));
        }
        /// <summary>
        /// Edit ticket
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        /// - replace:
        /// 
        ///         PATCH /api/patch/1
        ///           [{ "op": "replace", "path": "/accountId", "value": 3 }]
        ///
        /// - remove:
        /// 
        ///          PATCH /api/patch/1
        ///           [
        ///             { "op": "remove", "path": "/dateOpened" }
        ///           ]
        /// 
        /// - copy:
        /// 
        ///         PATCH /api/patch/1
        ///           [
        ///            { "op": "copy", "from": "/ticketNumber", "path": "/AccountId" }
        ///           ]
        ///
        ///  - add:
        /// 
        ///         PATCH /api/patch/1
        ///           [
        ///            { "op": "add", "path": "/dateOpened", "value":"2017/02/02" }
        ///           ]
        /// 
        /// - move:
        /// 
        ///         PATCH /api/patch/1
        ///           [
        ///            { "op": "move", "from": "/message", "path":"/text" }
        ///           ]
        /// 
        /// - test:
        /// 
        ///         PATCH /api/patch/1
        ///           [
        ///            {"op": "test", "path": "/state", "value": 0},
        ///            {"op": "replace", "path": "/state", "value": 2}
        ///           ]
        /// 
        /// </remarks>
        /// <param name="id">UID voor ticket dat je wil wijzigen.</param>
        /// <param name="dto">Een ticket-object met de waardes die gewijzigd moeten worden.</param>
        /// <returns>gewijzigde ticket.</returns>
        /// <response code="200">returns "Ok" als ticket gewijzigd is.</response>
        /// <response code="400">returns "BadRequest" wanneer er geen  ticket met id gevonden word</response>
        /// <response code="404">returns "NotFound" wanneer er geen  ticket met id gevonden word</response>
 
        [ProducesResponseType(typeof(TicketDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public IActionResult PatchTicket(int id, [FromBody] JsonPatchDocument<TicketDTO> dto)
        {
            var ticket = _mgr.GetTicket(id);
            if (ticket == null) return NotFound();

            var ticketToPatch =
                _mapper.Map<TicketDTO>(
                    ticket);

            dto.ApplyTo(ticketToPatch, ModelState);

            _mapper.Map(ticketToPatch,
                ticket);

            var updatedTicket = _mgr.ChangeTicket(ticket);

            if (updatedTicket == null) return BadRequest();
            return Ok(_mapper.Map<TicketDTO>(ticket));
        }
    }
}