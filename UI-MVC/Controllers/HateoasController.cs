using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.UI.Web.MVC.Models;
using SC.UI.Web.MVC.Models.DTOs;

namespace SC.UI.Web.MVC.Controllers
{
    [Route("api/[controller]")]
    public class HateoasController:Controller
    {
        private readonly IMapper _mapper;
        private readonly Helpers _helpers;
        private readonly ITicketManager _mgr = new TicketManager();

        public HateoasController(IMapper mapper)
        {
            _mapper = mapper;
            _helpers = new Helpers(mapper);
        }

        private TicketDTO GetTicketDto(int id)
        {
            return _mapper.Map<TicketDTO>(_mgr.GetTicket(id));
        }
        /// <summary>
        /// Get alle tickets
        /// </summary>
        /// <example>GET https://localhost:5001/api/hateoas</example>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/hateoas
        /// </remarks>
        /// <returns>Geeft een lijst met alle ticket objecten terug met de links per ticket en de globale links.</returns>
        /// <response code="200">Return OK wanneer lijst met tickets en links word gereturned </response>
        [ProducesResponseType(typeof(List<TicketDTO>), StatusCodes.Status200OK)]
        [Produces("application/json+hateoas")]
        [HttpGet(Name = "get-tickets")]
        
        public IActionResult GetTicket()
        {
            return Ok(_helpers.ConvertToTicketDtos(_mgr.GetTickets()));
        }
        /// <summary>
        /// Toont 1 ticket met id
        /// </summary>
        /// <param name="id">ticket UID</param>
        /// <example>GET https://localhost:5001/api/hateoas/1</example>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/hateoas/1 
        /// </remarks>
        /// <returns>Een specifiek ticket met al zijn links.</returns>
        /// <response code="200">OK wanneer ticket en links gereturned worden.</response>
        /// <response code="404">Geeft een NotFound als ticket niet gevonden word.</response>
        [Produces("application/json+hateoas")]
        [ProducesResponseType(typeof(TicketDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = "get-ticket")]
        
        public IActionResult GetTicket(int id)
        {
//            if (mgr.GetTicket(id) == null)
//            {
//                return NotFound("Ticket doesn't exist");
//            }
//
//            return Ok(GetTicketDto(id));
            return Ok(GetTicketDto(id));
        }
        /// <summary>
        /// Add new ticket
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /api/hateoas
        ///         {
        ///             "accountId" : 12,
        ///             "text" : "test"
        ///         }
        /// </remarks>
        /// <param name="ticket">nieuw ticket,inclusief ticketid en tekst.</param>
        /// <returns>ticket dat werdt toegevoegd.</returns>
        /// <response code="200">Returns OK als ticket is toegevoegd aan database.</response>
        [ProducesResponseType(typeof(TicketDTO), StatusCodes.Status200OK)]
        [Produces("application/json+hateoas")]
        [HttpPost(Name = "create-ticket")]
        public IActionResult PostTicket([FromBody] NewTicketDTO ticket)
        {
            return Ok(_mgr.AddTicket(ticket.AccountId, ticket.Text));
        }
        /// <summary>
        /// Update ticket
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /api/hateoas
        ///         {
        ///             "ticketNumber": 7,
        ///             "accountId": 12,
        ///             "text": "kapotte pc",
        ///              dateOpened": "2019-03-01T19:46:29.833Z",
        ///              "state": 1
        ///         }
        /// </remarks>
        /// <param name="id">UID voor ticket da je wil updaten.</param>
        /// <param name="ticket">Ticket met de waardes die gewijzigd moeten worden.</param>
        /// <returns>gewijzigde ticket.</returns>
        /// <response code="200">Returns OK als het gewijzigd is.</response>

        [ProducesResponseType(typeof(TicketDTO), StatusCodes.Status200OK)]
        [Produces("application/json+hateoas")]
        [HttpPut("{id}", Name = "update-ticket")]
        public IActionResult PutTicket(int id, [FromBody] TicketDTO ticket)
        {
            var ticketToUpdate = _mapper.Map(ticket, _mgr.GetTicket(id));

            return Ok(_mgr.ChangeTicket(ticketToUpdate));
        }
        /// <summary>
        /// Delete ticket
        /// </summary>
        /// 
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /api/hateoas/1
        /// </remarks>
        /// 
        /// <param name="id">UID voor ticket dat je wil verwijderen.</param>
        /// 
        /// <returns>verwijderde ticket.</returns>
        /// 
        /// <response code="200">Returns ok als het verwijderd is uit database.</response>

        [ProducesResponseType(typeof(TicketDTO), StatusCodes.Status200OK)]
        [Produces("application/json+hateoas")]
        [HttpDelete("{id}", Name = "delete-ticket")]
        public IActionResult DeleteTicket(int id)
        {
            return Ok(_mgr.RemoveTicket(id));
        }
    }
}