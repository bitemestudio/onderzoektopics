﻿using System.Collections;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SC.UI.Web.MVC.Models;

namespace SC.UI.Web.MVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Login()
        {
            ViewData["Message"] = "Login";

            return View();
        }
        public IActionResult Swagger()
        {
            ViewData["Message"] = "Swagger";

            return Redirect("/swagger");
        }

        public IActionResult Authentication(string Id)
        {
            ViewData["Message"] = "Login";
            ArrayList users;
            users = Tools.getUserList();

            User selectedUser = null;

            foreach (User user in users)
            {
                if (int.TryParse(Id, out var result))
                    if (user.id == result)
                        selectedUser = user;

                if (user.name == Id) selectedUser = user;
            }

            if (selectedUser == null) return RedirectToAction("Login");

            if (selectedUser.admin)
            {
                var user = new DataHolder.user();
                user.name = selectedUser.name;
                user.admin = true;
                DataHolder.UserDataList.Add(user);
                return RedirectToAction("AdminChat", selectedUser);
            }

            var userClient = new DataHolder.user();
            userClient.name = selectedUser.name;
            userClient.admin = false;
            DataHolder.UserDataList.Add(userClient);

            return RedirectToAction("ClientChat", selectedUser);
        }

        public IActionResult ClientChat()
        {
            ViewData["Message"] = "Login";
            return View();
        }

        public IActionResult AdminChat()
        {
            ViewData["Message"] = "Login";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
            });
        }
    }
}