﻿using System.Collections.Generic;
using SC.BL.Domain;

namespace SC.BL
{
    public interface ITicketManager
    {
        IEnumerable<Ticket> GetTickets();
        Ticket GetTicket(int ticketNumber);
        Ticket AddTicket(int accountId, string question);
        Ticket AddTicket(int accountId, string device, string problem);
        Ticket ChangeTicket(Ticket ticket);
        Ticket RemoveTicket(int ticketNumber);

        IEnumerable<TicketResponse> GetTicketResponses(int ticketNumber);
        TicketResponse AddTicketResponse(int ticketNumber, string response, bool isClientResponse);
    }
}